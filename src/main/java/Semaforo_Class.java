
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Canvas;
import java.awt.Color;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author flavi
 */
public class Semaforo_Class implements Runnable {

    SharedObject obj = new SharedObject();


    public Semaforo_Class(SharedObject obj) {
        this.obj = obj;
    }

    @Override
    public void run() {
        String myname = Thread.currentThread().getName();
        JFrame f = new JFrame(myname);
        Canvas cnvs = new Canvas();
        cnvs.setSize(400, 400);
        cnvs.setBackground(Color.GREEN);

        f.add(cnvs);
        f.setSize(200, 200);
        f.setLocation(1 * 200, 100);
        f.setVisible(true);
        
        while(obj.getSemaforoSem().availablePermits() <2){
            try {
                obj.getSemaforoSem().acquire();
                switch(obj.getColorSem()){
                    case 1:
                        cnvs.setBackground(Color.GREEN);
                        break;
                    case 2:
                        cnvs.setBackground(Color.RED);
                        break;
                }
                
                
                
            } catch (InterruptedException ex) {
                Logger.getLogger(Semaforo_Class.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        
        
        
        
    }

}
