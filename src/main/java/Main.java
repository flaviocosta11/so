
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author flavi
 */
public class Main implements Runnable {

    Semaphore sem;
    int TAM_PAR = 0;
    Semaphore lugaresLivres;
    SharedObject obj = new SharedObject();
<<<<<<< HEAD
    Sinal sinal = new Sinal();
=======
    int cancela = 0;
>>>>>>> master

    public Main(Semaphore sem, SharedObject obj, Sinal sinal) {
        this.sem = sem;
        this.obj = obj;
        this.sinal = sinal;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Semaphore sem = new Semaphore(0);
        Semaphore btnPermit = new Semaphore(1);
        Semaphore cancelaSem = new Semaphore(0);
        Semaphore semaforoSem = new Semaphore(0);

        Sinal sinal = new Sinal();

        SharedObject obj = new SharedObject();

        obj.setSem(btnPermit);
        obj.setSemaforoSem(semaforoSem);
        obj.setCancelaSem(cancelaSem);

        Cancela cancelaClass = new Cancela(obj, sinal);
        Semaforo_Class semaforoClass = new Semaforo_Class(obj);
        CartaoChave cartChClass = new CartaoChave(sem, obj);
        Main main = new Main(sem, obj, sinal);

        Thread cancela = new Thread(cancelaClass, "Cancela");
        Thread semaforo = new Thread(semaforoClass, "Semaforo");
        Thread cartao_ch = new Thread(cartChClass, "Cartão/Chave");
        Thread mainThread = new Thread(main, "Main");

        //Iniciar threads
        cancela.start();
        semaforo.start();
        cartao_ch.start();
        mainThread.start();
    }

    @Override
    public void run() {
        //ler lugares parque
        TAM_PAR = getLugaresParque();
        lugaresLivres = new Semaphore(TAM_PAR);
        //fim ler lugares parque

        String myname = Thread.currentThread().getName();
        JFrame frame = new JFrame(myname);
        JLabel lugares = new JLabel();
        JLabel cancelaEstado = new JLabel();

        cancelaEstado.setText(" ");
        frame.add(cancelaEstado, BorderLayout.EAST);
        lugares.setText(String.valueOf("Lugares disponiveis:" + lugaresLivres.availablePermits()));
        frame.add(lugares, BorderLayout.WEST);
        frame.setSize(300, 200);

        frame.setLocation(8 * 197, 100);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
<<<<<<< HEAD
        obj.setPosicaoCancela(2);
        obj.setColorSem(1);

=======

        obj.setPosicaoCancela(2);
        obj.setColorSem(1);
        obj.setPosicaoChave(0);
>>>>>>> master
        while (sem.availablePermits() < 2) {
            //obj.getList().get(2).setEnabled(false);

            try {
                sem.acquire();

                switch (obj.getBtnClicado()) {

                    case 1:
                        if (lugaresLivres.availablePermits() > 0) {
                            if (verificaCodCartao(obj.getCod_cartao())) {
                                GuardaConfig("Introduzido codigo de cartão valido.");
                                System.out.println("bingo");
                                cancelaEstado.setText("A abrir cancela... ");
<<<<<<< HEAD
                                obj.setPosicaoCancela(1);
                                obj.getCancelaSem().release();
                                sinal.espera();
                                cancelaEstado.setText("Cancela Aberta! ");
=======
                                Thread.sleep(2000);
                                cancelaEstado.setText("Cancela Aberta! ");

                                obj.setPosicaoCancela(1);
                                obj.getCancelaSem().release();
>>>>>>> master

                                LibertaBtns(obj, 6000);

                                GuardaConfig("Cancela aberta");
                            } else {
                                JOptionPane.showMessageDialog(null,
                                        "Codigo Invalido!"
                                        + "",
                                        "",
                                        JOptionPane.INFORMATION_MESSAGE);
                                GuardaConfig("Introduzido codigo de cartão invalido.");
                            }
                        } else {
                            ToastMessage message = new ToastMessage("PARQUE CHEIO!!!!");
                            message.display();
                            GuardaConfig("Tentativa de introdução de codigo de cartão com Parque lotado.");

                        }
                        LibertaBtns(obj, 1000);
                        break;
                    case 2:
                        if (obj.getPosicaoCancela() == 1 && obj.getPosicaoChave() == 0) {
                            GuardaConfig("Entrada de Carro.");
                            cancelaEstado.setText("A fechar cancela... ");
                            Thread.sleep(2000);
                            cancelaEstado.setText("Cancela Fechada! ");
                            obj.setPosicaoCancela(2);
                            obj.getCancelaSem().release();
                            lugaresLivres.acquire();
                            lugares.setText(String.valueOf("Lugares disponiveis:" + lugaresLivres.availablePermits()));
                            Thread.sleep(2000);
                            cancelaEstado.setText(" ");
                            if (lugaresLivres.availablePermits() == 0) {
                                obj.setColorSem(2);
                                obj.getSemaforoSem().release();
                            }
                            GuardaConfig("Cancela Fechada.");

                        } else if (obj.getPosicaoCancela() == 1 && obj.getPosicaoChave() == 1) {
                            if (lugaresLivres.availablePermits() == 0) {
                                JOptionPane.showMessageDialog(null,
                                        "Não é possivel entrar, pois o parque está cheio!"
                                        + "",
                                        "",
                                        JOptionPane.INFORMATION_MESSAGE);
                                GuardaConfig("Tentativa de entrada de veiculo com parque cheio registada.");

                            } else {
                                lugaresLivres.acquire();
                                lugares.setText(String.valueOf("Lugares disponiveis:" + lugaresLivres.availablePermits()));
                                if (lugaresLivres.availablePermits() == 0) {
                                    obj.setColorSem(2);
                                    obj.getSemaforoSem().release();
                                }
                            }
                        } else {
                            JOptionPane.showMessageDialog(null,
                                    "Para Dar entrada do carro é necessário passar cartão ou utilizar chave!"
                                    + "",
                                    "",
                                    JOptionPane.INFORMATION_MESSAGE);
                            GuardaConfig("Tentativa de abertura de cancela sem validação de cartão ou abertura com chave.");
                        }

                        LibertaBtns(obj, 2000);
                        //ativarBotoes();
                        break;
                    case 3:
                        if (obj.getPosicaoCancela() == 1 && obj.getPosicaoChave() == 0) {
                            if (lugaresLivres.availablePermits() == TAM_PAR) {
                                JOptionPane.showMessageDialog(null,
                                        "Não consta nenhum veiculo no parque para dar saida!"
                                        + "",
                                        "",
                                        JOptionPane.INFORMATION_MESSAGE);
                                GuardaConfig("Tentativa de saida de veiculo sem nenhum veiculo com entrada registada.");

                            } else {
                                GuardaConfig("Saida de Carro.");
                                cancelaEstado.setText("A fechar cancela... ");
                                Thread.sleep(2000);
                                cancelaEstado.setText("Cancela Fechada! ");
                                obj.setPosicaoCancela(2);
                                obj.getCancelaSem().release();
                                if (lugaresLivres.availablePermits() == 0) {
                                    obj.setColorSem(1);
                                    obj.getSemaforoSem().release();
                                }
                                lugaresLivres.release();
                                lugares.setText(String.valueOf("Lugares disponiveis:" + lugaresLivres.availablePermits()));
                                Thread.sleep(2000);
                                cancelaEstado.setText(" ");
                                GuardaConfig("Cancela Fechada.");
                            }

                        } else if (obj.getPosicaoCancela() == 1 && obj.getPosicaoChave() == 1) {
                            if (lugaresLivres.availablePermits() == TAM_PAR) {
                                JOptionPane.showMessageDialog(null,
                                        "Não consta nenhum veiculo no parque para dar saida!"
                                        + "",
                                        "",
                                        JOptionPane.INFORMATION_MESSAGE);
                                GuardaConfig("Tentativa de saida de veiculo sem nenhum veiculo com entrada registada.");

                            } else {
                                if (lugaresLivres.availablePermits() != 0) {
                                    lugaresLivres.release();
                                    lugares.setText(String.valueOf("Lugares disponiveis:" + lugaresLivres.availablePermits()));
                                } else if (lugaresLivres.availablePermits() == 0) {
                                    //obj.setColorSem(1);
                                    obj.setColorSem(1);
                                    obj.getSemaforoSem().release();
                                    lugaresLivres.release();
                                    lugares.setText(String.valueOf("Lugares disponiveis:" + lugaresLivres.availablePermits()));
                                }

                            }
                        } else {
                            JOptionPane.showMessageDialog(null,
                                    "Para Dar saida do carro é necessário passar cartão ou utilizar chave!"
                                    + "",
                                    "",
                                    JOptionPane.INFORMATION_MESSAGE);
                            GuardaConfig("Tentativa de saida de veiculo sem validação de cartão ou abertura com chave.");
                        }
                        LibertaBtns(obj, 2000);
                        break;
                    case 4:
                        if (obj.getPosicaoCancela() == 1) {
//                            GuardaConfig("");
                            cancelaEstado.setText("A fechar cancela... ");
                            Thread.sleep(2000);
                            cancelaEstado.setText("Cancela Fechada! ");
                            obj.setPosicaoCancela(2);
                            obj.getCancelaSem().release();
                            Thread.sleep(2000);
                            cancelaEstado.setText(" ");
                            GuardaConfig("Cancela Fechada.");
                            obj.setPosicaoChave(0);

                        } else if (obj.getPosicaoCancela() == 2) {
//                            GuardaConfig("");
                            cancelaEstado.setText("A abrir cancela... ");
                            Thread.sleep(2000);
                            cancelaEstado.setText("Cancela Aberta! ");
                            obj.setPosicaoCancela(1);
                            obj.getCancelaSem().release();
                            Thread.sleep(2000);
                            cancelaEstado.setText(" ");
                            GuardaConfig("Cancela Aberta.");
                            obj.setPosicaoChave(1);

                        }
                        LibertaBtns(obj, 2000);
                        break;
                    case 5:
                        if (lugaresLivres.availablePermits() == TAM_PAR && obj.getPosicaoCancela() == 2) {
                            JOptionPane.showMessageDialog(null,
                                    "Sistema já está reseted"
                                    + "",
                                    "",
                                    JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            GuardaConfig("Reset do sistema.");
                            while (lugaresLivres.availablePermits() < TAM_PAR) {
                                lugaresLivres.release();
                            }
                            cancelaEstado.setText("A fechar cancela... ");
                            Thread.sleep(2000);
                            cancelaEstado.setText("Cancela Fechada! ");
                            obj.setPosicaoCancela(2);
                            obj.getCancelaSem().release();

                            lugares.setText(String.valueOf("Lugares disponiveis:" + lugaresLivres.availablePermits()));
                            obj.setColorSem(1);
                            Thread.sleep(2000);
                            cancelaEstado.setText(" ");
                            //obj.setSemaforoVerde();

                        }
                        LibertaBtns(obj, 2000);
                        break;
                    case 6:
                        if (obj.getColorSem() == 1) {
                            obj.setColorSem(2);
                            obj.getSemaforoSem().release();

                        } else if (obj.getColorSem() == 2) {
                            obj.setColorSem(1);
                            obj.getSemaforoSem().release();

                        }
                        break;
                }

                System.out.println("fui clicado");
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void LibertaBtns(SharedObject obj, int tmp) {
        Thread thread = new Thread("New Thread") {
            public void run() {
                try {

                    Thread.sleep(tmp);
                    obj.getSem().release();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        thread.start();
    }

    /*
    public void ativarBotoes() {
        Thread thread = new Thread("New Thread") {
            public void run() {
                try {

                    Thread.sleep(6000);
                    //obj.getButton().setEnabled(true);
                    int saida;
                    int entrada;

                    for (int i = 0; i < obj.getList().size() - 2; i++) {
                        if (obj.getList().get(i).getName().equals("sd_carro")) {
                            saida = i;
                        } else if (obj.getList().get(i).getName().equals("entr_carro")) {
                            entrada = i;
                        }

                    }

                    if (lugaresLivres.availablePermits() > 0) {
                        for (int i = 0; i < obj.getList().size() - 2; i++) {
                            if (obj.getList().get(i).getName().equals("sd_carro")) {
                                obj.getList().get(i).setEnabled(false);
                            } else {
                                obj.getList().get(i).setEnabled(true);
                            }

                        }

                        System.out.println(obj.getList().get(3).getName());

                    }
                    if (lugaresLivres.availablePermits() < TAM_PAR) {
                        obj.getList().get(1).setEnabled(true);
                    }

                    obj.getList().get(4).setEnabled(true);
                    obj.getSem().release();

                } catch (InterruptedException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        thread.start();
    }
     */
    public boolean verificaCodCartao(int cod) {
        BufferedReader br = null;

        try {
            boolean numCarta = false;
            String linha;
            br = new BufferedReader(new FileReader("config.txt"));

            while ((linha = br.readLine()) != null) {
                if (linha.equals("fim_codigos_cartoes")) {
                    break;
                }
                if (linha.equals("codigos_cartoes")) {
                    numCarta = true;

                } else if (numCarta == true) {
                    //System.out.println(cod);
                    if (cod == Integer.valueOf(linha)) {
                        return true;
                    }
                }

                System.out.println("linha : " + linha);
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ioex) {
                ioex.printStackTrace();
            }
        }

        return false;
    }

    public int getLugaresParque() {
        BufferedReader br = null;

        try {
            boolean numCarta = false;
            String linha;
            br = new BufferedReader(new FileReader("config.txt"));

            while ((linha = br.readLine()) != null) {
                if (linha.equals("fim_lugares")) {
                    break;
                }
                if (linha.equals("lugares_parque")) {
                    numCarta = true;

                } else if (numCarta == true) {
                    return Integer.valueOf(linha);
                }

                System.out.println("linha : " + linha);
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ioex) {
                ioex.printStackTrace();
            }
        }

        return 0;
    }

    public void GuardaConfig(String log) throws IOException {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        Date date = new Date();
        BufferedWriter output;
        output = new BufferedWriter(new FileWriter("log.txt", true));
        String sizeX = log;
        output.append(sizeX + " " + dateFormat.format(date));
        output.newLine();
        output.close();
    }

}
