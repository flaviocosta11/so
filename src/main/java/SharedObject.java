
import java.awt.Canvas;
import java.awt.Color;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author flavi
 */
class SharedObject {

    int btnClicado = 0;
    int cod_cartao;

    Semaphore sem;

    Semaphore CancelaSem;
    int PosicaoCancela;
    int PosicaoChave;
    Semaphore SemaforoSem;
    int ColorSem;

    SharedObject() {
    }

    public int getBtnClicado() {
        return btnClicado;
    }

    public void setBtnClicado(int btnClicado) {
        this.btnClicado = btnClicado;
    }

    public int getCod_cartao() {
        return cod_cartao;
    }

    public void setCod_cartao(int cod_cartao) {
        this.cod_cartao = cod_cartao;
    }

    public Semaphore getSem() {
        return sem;
    }

    public void setSem(Semaphore sem) {
        this.sem = sem;
    }

    public Semaphore getCancelaSem() {
        return CancelaSem;
    }

    public void setCancelaSem(Semaphore CancelaSem) {
        this.CancelaSem = CancelaSem;
    }

    public Semaphore getSemaforoSem() {
        return SemaforoSem;
    }

    public void setSemaforoSem(Semaphore SemaforoSem) {
        this.SemaforoSem = SemaforoSem;
    }

    public int getPosicaoCancela() {
        return PosicaoCancela;
    }

    public void setPosicaoCancela(int PosicaoCancela) {
        this.PosicaoCancela = PosicaoCancela;
    }

    public int getColorSem() {
        return ColorSem;
    }

    public void setColorSem(int ColorSem) {
        this.ColorSem = ColorSem;
    }

    public int getPosicaoChave() {
        return PosicaoChave;
    }

    public void setPosicaoChave(int PosicaoChave) {
        this.PosicaoChave = PosicaoChave;
    }

}
