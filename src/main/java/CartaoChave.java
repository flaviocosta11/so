
import java.util.List;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author flavi
 */
public class CartaoChave implements Runnable {

    Semaphore sem;
    SharedObject obj = new SharedObject();
    Semaphore btnPermit;

    public CartaoChave(Semaphore sem, SharedObject obj) {
        this.sem = sem;
        this.obj = obj;
    }

    @Override
    public void run() {
        String myname = Thread.currentThread().getName();
        JFrame f = new JFrame(myname);
        btnPermit = obj.getSem();
        JButton psg_cartao = new JButton("Passagem Cartão");
        psg_cartao.setName("psg_cartao");
        psg_cartao.setBounds(50, 100, 150, 30);
        JButton entr_carro = new JButton("Entrada de Carro");
        entr_carro.setName("entr_carro");
        entr_carro.setBounds(210, 100, 150, 30);
        //entr_carro.setEnabled(false);
        JButton sd_carro = new JButton("Saida Carro");
        sd_carro.setName("sd_carro");
        sd_carro.setBounds(370, 100, 110, 30);
        //sd_carro.setEnabled(false);

        JButton paraImediata = new JButton("Paragem Imediata Sistema");
        paraImediata.setBounds(200, 150, 180, 30);
        paraImediata.setName("paraImediata");

        JButton chave = new JButton("CHAVE");
        chave.setBounds(490, 100, 110, 30);
        chave.setName("chave");

        JButton reset = new JButton("Reset do Sistema");
        reset.setBounds(610, 100, 150, 30);
        reset.setName("reset");
        
        psg_cartao.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {
                    btnPermit.acquire();
                } catch (InterruptedException ex) {
                    Logger.getLogger(CartaoChave.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (obj.getPosicaoCancela() == 1) {
                    ToastMessage message = new ToastMessage("Já abriu a cancela!");
                    message.display();
                    btnPermit.release();
                } else {
                    obj.setBtnClicado(1);

                    String input = null;
                    int countC = 0;
                    do {
                        input = JOptionPane.showInputDialog("codigo 4 digitos: ");

                        if (input == null) {
                            int xx = JOptionPane.showConfirmDialog(null,
                                    "Tem certeza que deseja cancelar?",
                                    "",
                                    JOptionPane.YES_NO_OPTION);
                            if (xx == JOptionPane.YES_OPTION) {
                                btnPermit.release();
                                JOptionPane.showMessageDialog(null,
                                        "A sair...."
                                        + "",
                                        "",
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;

                                //System.exit(0);
                            } else {
                            }

                        }

                        if (input != null) {
                            countC = input.length();//contar caractres introduzidos
                        }
                    } while (countC < 4 || countC > 4);
                    if (input != null) {
                        obj.setCod_cartao(Integer.valueOf(input));
                        sem.release();
                    }
                }

            }
        });

        entr_carro.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {
                    btnPermit.acquire();
                } catch (InterruptedException ex) {
                    Logger.getLogger(CartaoChave.class.getName()).log(Level.SEVERE, null, ex);
                }
                obj.setBtnClicado(2);
                sem.release();

            }
        });

        sd_carro.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {
                    btnPermit.acquire();
                } catch (InterruptedException ex) {
                    Logger.getLogger(CartaoChave.class.getName()).log(Level.SEVERE, null, ex);
                }
                obj.setBtnClicado(3);
                sem.release();

            }
        });

        chave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {
                    btnPermit.acquire();
                } catch (InterruptedException ex) {
                    Logger.getLogger(CartaoChave.class.getName()).log(Level.SEVERE, null, ex);
                }         
                obj.setBtnClicado(4);
                sem.release();

            }
        });

        reset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {
                    btnPermit.acquire();
                } catch (InterruptedException ex) {
                    Logger.getLogger(CartaoChave.class.getName()).log(Level.SEVERE, null, ex);
                }
                obj.setBtnClicado(5);
                sem.release();

            }
        });

        paraImediata.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (btnPermit.availablePermits() == 1) {
                    try {
                        btnPermit.acquire();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(CartaoChave.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    psg_cartao.setEnabled(false);
                    entr_carro.setEnabled(false);
                    sd_carro.setEnabled(false);
                    chave.setEnabled(false);
                    reset.setEnabled(false);
                } else {
                    psg_cartao.setEnabled(true);
                    entr_carro.setEnabled(true);
                    sd_carro.setEnabled(true);
                    chave.setEnabled(true);
                    reset.setEnabled(true);
                    btnPermit.release();

                }

                //System.out.println(" " + btnPermit.availablePermits());
                obj.setBtnClicado(6);
                sem.release();

            }
        });

        f.add(psg_cartao);
        f.add(entr_carro);
        f.add(sd_carro);
        f.add(chave);
        f.add(reset);
        f.add(paraImediata);
        //f.add(tf);
        f.setSize(820, 400);
        f.setLayout(null);
        f.setLocation(3 * 250, 100);
        f.setVisible(true);
    }
}
