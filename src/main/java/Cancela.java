
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author flavi
 */
public class Cancela implements Runnable {

    SharedObject obj = new SharedObject();
    Sinal sinal = new Sinal();

    public Cancela(SharedObject obj,Sinal sinal) {
        this.obj = obj;
        this.sinal = sinal;
    }

    @Override
    public void run() {
        String myname = Thread.currentThread().getName();
        JFrame f = new JFrame(myname);
        JLabel lugares = new JLabel();
        JLabel cancelaEstado = new JLabel();
        lugares.setText("Cancela Fechada");
        f.add(lugares);
        f.setSize(350, 350);
        f.setLocation(2 * 200, 100);
        f.setVisible(true);
        obj.setPosicaoCancela(2);

        while (obj.getCancelaSem().availablePermits() < 2) {
            try {
                obj.getCancelaSem().acquire();
                switch (obj.getPosicaoCancela()) {
                    case 1:
                        Thread.sleep(5000);
                        lugares.setText("Cancela Aberta");
                        sinal.acorda();
                        break;
                    case 2:
                        Thread.sleep(5000);
                        lugares.setText("Cancela Fechada");
                        sinal.acorda();
                        break;
                }

            } catch (InterruptedException ex) {
                Logger.getLogger(Semaforo_Class.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
